import scrapy, json, datetime
from insert import insert_item


class ItemSetSpider(scrapy.Spider):
    name = "itemset"
    start_urls = ['https://news.ycombinator.com']

    with open('datademo.txt', 'a') as outfile:

        def parse(self, response):

            data = {}
            data['itemset'] = []

            SET_SELECTOR = '.athing'
            SET_subSELECTOR = 'tr .subtext'


            with open('datademo.txt', 'a') as outfile:
                for item in response.css(SET_SELECTOR):

                    ID_SELECTOR = '::attr(id)'
                    id = item.css(ID_SELECTOR).extract_first()

                    RANK_SELECTOR = '.rank ::text'
                    TITLE_SELECTOR = '.title a::text'
                    LINK_SELECTOR = '.title a::attr(href)'

                    rank = item.css(RANK_SELECTOR).extract_first()
                    title = item.css(TITLE_SELECTOR).extract_first()
                    link = item.css(LINK_SELECTOR).extract_first()
                    score = ''
                    date = ''

                    for subItem in response.css(SET_subSELECTOR):
                        scoreID_SELECTOR = '.score ::attr(id)'
                        scoreID = subItem.css(scoreID_SELECTOR).extract_first()
                        ageID = subItem.css('.age a ::attr(href)').get()
                        if str(id) in str(scoreID) or str(id) in str(ageID):
                            SCORE_SELECTOR = '.score ::text'
                            AGE_SELECTOR = '.age ::text'
                            score = subItem.css(SCORE_SELECTOR).extract_first()
                            time = subItem.css(AGE_SELECTOR).extract_first()
                            break


                    """ convert time_ago to datetime """

                    time_extract = int(''.join(number for number in date if number.isdigit()))
                    day_time = datetime.datetime.now()
                    # print(time_extract)

                    if 'min' in date:
                        day_time = day_time - datetime.timedelta(minutes=time_extract)
                    if 'hour' in date:
                        day_time = day_time - datetime.timedelta(hours=time_extract)
                    if 'day' in date:
                        day_time = day_time - datetime.timedelta(days=time_extract)
                    if 'week' in date:
                        day_time = day_time - datetime.timedelta(weeks=time_extract)

                    data['itemset'].append({
                        'rank': rank,
                        'title': title,
                        'link': link,
                        'score': score,
                        'date': str(day_time)
                    })

            json.dump(data, outfile, indent=4)

            NEXT_PAGE_SELECTOR = '.morelink ::attr(href)'
            next_page = response.css(NEXT_PAGE_SELECTOR).extract_first()
            if next_page:
                yield scrapy.Request(
                    response.urljoin('https://news.ycombinator.com/' + next_page),
                    callback=self.parse
                )
