import datetime

def ago(t):
    """
        Calculate a '3 hours ago' type string from a python datetime.
    """
    units = {
        'days': lambda diff: diff.days,
        'hours': lambda diff: diff.seconds / 3600,
        'minutes': lambda diff: diff.seconds % 3600 / 60,
    }
    diff = datetime.datetime.now() - t
    for unit in units:
        dur = units[unit](diff) # Run the lambda function to get a duration
        if dur > 0:
            unit = unit[:-dur] if dur == 1 else unit # De-pluralize if duration is 1 ('1 day' vs '2 days')
            return '%s %s ago' % (dur, unit)
    return 'just now'