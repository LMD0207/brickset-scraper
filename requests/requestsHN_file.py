import urllib.request, json
import datetime
from parsel import Selector
from insert import insert_item

global url
url = 'https://news.ycombinator.com'


# r = requests.get('https://news.ycombinator.com')

class ItemSetRe:

    def parse(self, url):
        r = urllib.request.urlopen(url).read()
        r = r.decode("utf-8")
        response = Selector(r)

        data = {}
        data['itemset'] = []
        with open('re_data.txt', 'a') as outfile:
            for item in response.css('.athing'):

                id = item.css('::attr(id)').get()
                rank = item.css('.rank ::text').get()
                title = item.css('.title a ::text').get()
                link = item.css('.title a ::attr(href)').get()
                score = ""
                date = ""

                for subItem in response.css('.subtext'):
                    score_id = subItem.css('.score ::attr(id)').get()
                    age_id = subItem.css('.age a ::attr(href)').get()
                    if str(id) in str(score_id) or str(id) in str(age_id):
                        score = subItem.css('.score ::text').get()
                        date = subItem.css('.age ::text').get()
                        break


                print(id)
                print(rank)
                print(title)
                print(link)
                print(score)
                #print(date)


                """ convert time_ago to datetime """

                time_extract = int(''.join(number for number in date if number.isdigit()))
                day_time = datetime.datetime.now()
                # print(time_extract)

                if 'min' in date:
                    day_time = day_time - datetime.timedelta(minutes=time_extract)
                if 'hour' in date:
                    day_time = day_time - datetime.timedelta(hours=time_extract)
                if 'day' in date:
                    day_time = day_time - datetime.timedelta(days=time_extract)
                if 'week' in date:
                    day_time = day_time - datetime.timedelta(weeks=time_extract)

                print(day_time)
                print('\n')

                data['itemset'].append({
                    'rank': rank,
                    'title': title,
                    'link': link,
                    'score': score,
                    'date': str(day_time),
                })

            json.dump(data, outfile, indent=4)

        next_page = response.css('.morelink ::attr(href)').get()
        if next_page:
            url = 'https://news.ycombinator.com/' + next_page
            callback = self.parse(url)


ItemSetRe().parse(url)
