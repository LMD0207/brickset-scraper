import pymysql


def insert_item(rank, title, link, score, create_time):
    sql = "INSERT INTO itemset(r, title, link, score, create_time)\
        VALUES('%s','%s','%s','%s','%s')" % \
          (rank, title, link, score, create_time)

    """ Connect to MySQL database """
    db = pymysql.connect("localhost", "root", "root1234", "r_itemset")
    cursor = db.cursor()

    try:
        cursor.execute(sql)
        db.commit()
    except:
        db.rollback()


def main():
    insert_item('1', 'hello', 'hello', '1', 'now')


if __name__ == '__main__':
    main()
