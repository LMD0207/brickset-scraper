import urllib.request, json, datetime, time
from parsel import Selector
from insert import insert_item

global url
url = 'https://news.ycombinator.com'


# r = requests.get('https://news.ycombinator.com')

class ItemSetRe:

    def parse(self, url):
        r = urllib.request.urlopen(url).read()
        r = r.decode("utf-8")
        response = Selector(r)

        data = {}
        data['itemset'] = []

        for item in response.css('.athing'):

            id = item.css('::attr(id)').get()
            #print(id)
            rank = item.css('.rank ::text').get()
            title = item.css('.title a ::text').get()
            link = item.css('.title a ::attr(href)').get()
            score = ""
            date = ""

            for subItem in response.css('.subtext'):
                score_id = subItem.css('.score ::attr(id)').get()
                if str(id) in str(score_id):
                    score = subItem.css('.score ::text').get()
                    date = subItem.css('.age ::text').get()
                    break

            insert_item(rank, title, link, score, date)
            data['itemset'].append({
                'rank': rank,
                'title': title,
                'link': link,
                'score': score,
                'date': date
            })

            print(id)
            print(rank)
            print(title)
            print(link)
            print(score)
            print(date)


            time_extract = int(''.join(c for c in date if c.isdigit()))
            day_time = datetime.datetime.now()
            #print(time_extract)

            if 'min' in date:
                day_time = day_time - datetime.timedelta(minutes=time_extract)
            if 'hour' in date:
                day_time = day_time - datetime.timedelta(hours=time_extract)
            if 'day' in date:
                day_time = day_time - datetime.timedelta(days=time_extract)
            if 'week' in date:
                day_time = day_time - datetime.timedelta(weeks=time_extract)

            print(day_time)
            print('\n')

ItemSetRe().parse(url)

test1 = '2 days ago'
test2 = '30 minutes ago'
